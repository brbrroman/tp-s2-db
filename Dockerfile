FROM golang:1.12-stretch AS builder

WORKDIR /app

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY app/ ./app/
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 GOGC=off go build -a -installsuffix cgo -ldflags="-w -s" app/main.go

FROM ubuntu:19.04
ENV DEBIAN_FRONTEND=noninteractive 
ENV PORT 5000
ENV PGVER 11
ENV POSTGRES_HOST /var/run/postgresql/
ENV POSTGRES_PORT 5432
ENV POSTGRES_DB docker
ENV POSTGRES_USER docker
ENV POSTGRES_PASSWORD docker
EXPOSE $PORT

RUN apt-get update && apt-get install -y postgresql-$PGVER

USER postgres

RUN service postgresql start &&\
    psql --command "CREATE USER docker WITH SUPERUSER PASSWORD 'docker';" &&\
    createdb -O $POSTGRES_USER $POSTGRES_DB &&\
    service postgresql stop

COPY config/pg_hba.conf /etc/postgresql/$PGVER/main/pg_hba.conf
# COPY config/postgresql.conf /etc/postgresql/$PGVER/main/postgresql.conf

VOLUME  ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]

COPY sql/ sql/
COPY --from=builder /app/main .
CMD service postgresql start && ./main