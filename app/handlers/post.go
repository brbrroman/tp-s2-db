package handlers

import (
	"strconv"
	"strings"

	"tp-s2-db/app/helpers"
	"tp-s2-db/app/models"

	"github.com/pquerna/ffjson/ffjson"
	"github.com/valyala/fasthttp"
)

// PostDetailsGET - Получение информации о ветке обсуждения по его имени
func PostDetailsGET(ctx *fasthttp.RequestCtx) {
	id, _ := strconv.Atoi(ctx.UserValue("id").(string))
	related := string(ctx.QueryArgs().Peek("related"))
	items := []string{""}
	items = append(items, strings.Split(related, ",")...)
	res, err := helpers.DBPostByID(id, &items)
	resp, _ := ffjson.Marshal(res)
	ErrorMux(ctx, &resp, &err, fasthttp.StatusOK)
}

// PostDetailsPOST - Изменение сообщения. Если сообщение поменяло текст, то оно должно получить отметку isEdited.
func PostDetailsPOST(ctx *fasthttp.RequestCtx) {
	id, _ := strconv.Atoi(ctx.UserValue("id").(string))
	var post models.Post
	if err := ffjson.Unmarshal(ctx.PostBody(), &post); err != nil {
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}
	res, err := helpers.DBPostUpdateByID(id, &post)
	resp, _ := ffjson.Marshal(res)
	ErrorMux(ctx, &resp, &err, fasthttp.StatusOK)
}
