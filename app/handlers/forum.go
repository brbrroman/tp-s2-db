package handlers

import (
	"tp-s2-db/app/helpers"
	"tp-s2-db/app/models"

	"github.com/pquerna/ffjson/ffjson"
	"github.com/valyala/fasthttp"
)

// ForumCreate - Добавление форума
func ForumCreate(ctx *fasthttp.RequestCtx) {
	var forum models.Forum
	if err := ffjson.Unmarshal(ctx.PostBody(), &forum); err != nil {
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}
	res, err := helpers.DBForumCreate(&forum)
	resp, _ := ffjson.Marshal(res)
	ErrorMux(ctx, &resp, &err, fasthttp.StatusCreated)
}

// ForumSlugCreate - Добавление новой ветки обсуждения на форум
func ForumSlugCreate(ctx *fasthttp.RequestCtx) {
	var thread models.Thread
	if err := ffjson.Unmarshal(ctx.PostBody(), &thread); err != nil {
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}
	slug := ctx.UserValue("slug").(string)
	thread.Forum = &slug
	res, err := helpers.DBThreadCreate(&thread)
	resp, _ := ffjson.Marshal(res)
	ErrorMux(ctx, &resp, &err, fasthttp.StatusCreated)
}

// ForumSlugDetails - Получение информации о форуме по его идентификаторе
func ForumSlugDetails(ctx *fasthttp.RequestCtx) {
	slug := ctx.UserValue("slug").(string)
	res, err := helpers.DBForumBySlug(slug)
	resp, _ := ffjson.Marshal(res)
	ErrorMux(ctx, &resp, &err, fasthttp.StatusOK)
}

// ForumSlugThreads - Получение списка ветвей обсужления данного форума. Ветви обсуждения выводятся отсортированные по дате создания. GET
func ForumSlugThreads(ctx *fasthttp.RequestCtx) {
	slug := ctx.UserValue("slug").(string)
	limit, since, desc := ctx.QueryArgs().Peek("limit"), ctx.QueryArgs().Peek("since"), ctx.QueryArgs().Peek("desc")
	res, err := helpers.DBThreadsBySlugLimit(slug, limit, since, desc)
	resp, _ := ffjson.Marshal(res)
	ErrorMux(ctx, &resp, &err, fasthttp.StatusOK)
}

// ForumSlugUsers - Получение списка пользователей, у которых есть пост или ветка обсуждения в данном форуме. Пользователи выводятся отсортированные по nickname в порядке возрастания. Порядок сотрировки должен соответсвовать побайтовому сравнение в нижнем регистре.
func ForumSlugUsers(ctx *fasthttp.RequestCtx) {
	slug := ctx.UserValue("slug").(string)
	limit, since, desc := ctx.QueryArgs().Peek("limit"), ctx.QueryArgs().Peek("since"), ctx.QueryArgs().Peek("desc")
	res, err := helpers.DBUsersBySlugLimit(slug, limit, since, desc)
	resp, _ := ffjson.Marshal(res)
	ErrorMux(ctx, &resp, &err, fasthttp.StatusOK)
}
