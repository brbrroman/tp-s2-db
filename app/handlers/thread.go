package handlers

import (
	"github.com/pquerna/ffjson/ffjson"
	"github.com/valyala/fasthttp"

	"tp-s2-db/app/helpers"
	"tp-s2-db/app/models"
)

// ThreadCreate - Добавление новых постов в ветку обсуждения на форум.
func ThreadCreate(ctx *fasthttp.RequestCtx) {
	slugOrID := ctx.UserValue("slug_or_id").(string)
	var posts models.Posts
	if err := ffjson.Unmarshal(ctx.PostBody(), &posts); err != nil {
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}
	res, err := helpers.DBPostsCreate(&posts, slugOrID)
	resp, _ := ffjson.Marshal(res)
	ErrorMux(ctx, &resp, &err, fasthttp.StatusCreated)
}

// ThreadDetailsGET - Получение информации о ветке обсуждения по его имени
func ThreadDetailsGET(ctx *fasthttp.RequestCtx) {
	slugOrID := ctx.UserValue("slug_or_id").(string)
	res, err := helpers.DBThreadBySlug(slugOrID)
	resp, _ := ffjson.Marshal(res)
	ErrorMux(ctx, &resp, &err, fasthttp.StatusOK)
}

// ThreadDetailsPOST - Обновление ветки обсуждения на форуме
func ThreadDetailsPOST(ctx *fasthttp.RequestCtx) {
	slugOrID := ctx.UserValue("slug_or_id").(string)

	var thread models.Thread
	if err := ffjson.Unmarshal(ctx.PostBody(), &thread); err != nil {
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}

	res, err := helpers.DBUpdateThreadBySlug(slugOrID, &thread)
	resp, _ := ffjson.Marshal(res)
	ErrorMux(ctx, &resp, &err, fasthttp.StatusOK)
}

// ThreadPosts - Сообщения данной ветви обсуждения. Сообщения выводятся отсортированные по дате создания.
func ThreadPosts(ctx *fasthttp.RequestCtx) {
	slugOrID := ctx.UserValue("slug_or_id").(string)
	limit, since, desc, sort := ctx.QueryArgs().Peek("limit"), ctx.QueryArgs().Peek("since"), ctx.QueryArgs().Peek("desc"), ctx.QueryArgs().Peek("sort")
	var res *models.Posts
	var err error
	res, err = helpers.DBPostsGetByThreadSlugOrID(&sort, slugOrID, limit, since, desc)
	resp, _ := ffjson.Marshal(res)
	ErrorMux(ctx, &resp, &err, fasthttp.StatusOK)
}

// ThreadVote - Изменение голоса за ветвь обсуждения. Один пользователь учитывается только один раз и может изменить своё мнение.
func ThreadVote(ctx *fasthttp.RequestCtx) {
	slugOrID := ctx.UserValue("slug_or_id").(string)
	var vote models.Vote
	if err := ffjson.Unmarshal(ctx.PostBody(), &vote); err != nil {
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}
	res, err := helpers.DBVote(slugOrID, &vote)
	resp, _ := ffjson.Marshal(res)
	ErrorMux(ctx, &resp, &err, fasthttp.StatusOK)
}
