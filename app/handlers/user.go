package handlers

import (
	"tp-s2-db/app/helpers"
	"tp-s2-db/app/models"

	"github.com/pquerna/ffjson/ffjson"
	"github.com/valyala/fasthttp"
)

// UserCreate - Создание нового пользователя
func UserCreate(ctx *fasthttp.RequestCtx) {
	var user models.User
	nickname := ctx.UserValue("nickname").(string)
	user.Nickname = &nickname
	if err := ffjson.Unmarshal(ctx.PostBody(), &user); err != nil {
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}
	res, err := helpers.DBUserCreate(&user)

	switch err {
	case nil:
		resp, _ := ffjson.Marshal(res)
		ctx.SetBody(resp[1 : len(resp)-1]) // Костыль, т.к. если ошибка = дубликат, то ожидается слайс из 1 элемента.
		ctx.SetStatusCode(fasthttp.StatusCreated)
	case models.EUserAE:
		resp, _ := ffjson.Marshal(res)
		ctx.SetBody(resp)
		ctx.SetStatusCode(fasthttp.StatusConflict)
	default:
		ctx.WriteString(err.Error())
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
	}
} //ALEEEEEEEEEEEERRRRRRRRRRTTTTTTTTTTT!!!!!!!!!!

// UserUpdate - Изменение информации в профиле пользователя
func UserUpdate(ctx *fasthttp.RequestCtx) {
	var user models.User
	nickname := ctx.UserValue("nickname").(string)
	user.Nickname = &nickname
	if err := ffjson.Unmarshal(ctx.PostBody(), &user); err != nil {
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		ctx.WriteString(err.Error())
		return
	}
	res, err := helpers.DBUserUpdate(&user)
	resp, _ := ffjson.Marshal(res)
	ErrorMux(ctx, &resp, &err, fasthttp.StatusOK)
}

// UserInfo - Получение информации о пользователе форума по его имени
func UserInfo(ctx *fasthttp.RequestCtx) {
	nickname := ctx.UserValue("nickname").(string)
	res, err := helpers.DBUserByNickname(nickname)
	resp, _ := ffjson.Marshal(res)
	ErrorMux(ctx, &resp, &err, fasthttp.StatusOK)
}
