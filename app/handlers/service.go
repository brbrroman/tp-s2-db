package handlers

import (
	"tp-s2-db/app/helpers"

	"github.com/pquerna/ffjson/ffjson"
	"github.com/valyala/fasthttp"
)

// ServiceClear - Безвозвратное удаление всей пользовательской информации из базы данных
func ServiceClear(ctx *fasthttp.RequestCtx) {
	helpers.DBClear()
}

// ServiceStatus - Получение инфомарции о базе данных
func ServiceStatus(ctx *fasthttp.RequestCtx) {
	res, err := helpers.DBStatus()
	resp, _ := ffjson.Marshal(res)
	ErrorMux(ctx, &resp, &err, fasthttp.StatusOK)
}
