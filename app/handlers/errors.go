package handlers

import (
	"log"
	"tp-s2-db/app/models"

	"github.com/pquerna/ffjson/ffjson"
	"github.com/valyala/fasthttp"
)

// ErrorMux - Мультиплексор ошибок
func ErrorMux(ctx *fasthttp.RequestCtx, resp *[]byte, err *error, statusCode int) {
	switch *err {
	case nil:
		ctx.SetBody(*resp)
		ctx.SetStatusCode(statusCode)
	case models.EForumNE,
		models.EThreadNE,
		models.EPostNE,
		models.EUserNE,
		models.ENE:
		resp, _ := ffjson.Marshal(*err)
		ctx.SetBody(resp)
		ctx.SetStatusCode(fasthttp.StatusNotFound)
	case models.EForumAE,
		models.EThreadAE,
		models.EPostAE,
		models.EUserAE,
		models.EWrongParent:
		ctx.SetBody(*resp)
		ctx.SetStatusCode(fasthttp.StatusConflict)
	default:
		log.Println((*err).Error())
		ctx.WriteString((*err).Error())
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
	}
}
