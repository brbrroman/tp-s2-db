package db

import (
	"io/ioutil"
	"gopkg.in/yaml.v2"
	"log"
	"os"

	"github.com/jackc/pgx"
)

var Database *pgx.ConnPool
var Perf = 0

type Prep map[string]string

// Open - инициализация подключения к БД
func Open() error {
	postgresConfig := pgx.ConnConfig{
		Host:     os.Getenv("POSTGRES_HOST"),
		Port:     5432,
		Database: os.Getenv("POSTGRES_DB"),
		User:     os.Getenv("POSTGRES_USER"),
		Password: os.Getenv("POSTGRES_PASSWORD"),
	}

	var err error
	Database, err = pgx.NewConnPool(pgx.ConnPoolConfig{
		ConnConfig:     postgresConfig,
		MaxConnections: 100,
	})
	if err != nil {
		return err
	}
	// Создание таблиц
	query, err := ioutil.ReadFile("sql/create.sql")
	if err != nil {
		return err
	}
	tx, err := Database.Begin()
	defer tx.Rollback()
	if err != nil {
		return err
	}
	if _, err := tx.Exec(string(query)); err != nil {
		return err
	}
	tx.Commit()

	jsonFile, err := os.Open("sql/prepare.yml")
	defer jsonFile.Close()
	if err != nil {
		return err
	}

	byteValue, _ := ioutil.ReadAll(jsonFile)
	var stmts Prep
	if err := yaml.Unmarshal(byteValue, &stmts); err != nil {
		return err
	}

	for key, value := range stmts {
		if _, err := Database.Prepare(key, value); err != nil {
			return err
		}
	}
	log.Println("db: Connection: Initialized")
	return nil
}

// Close - Закрытие подключения к БД
func Close() {
	log.Println("db: Connection: Closed")
	Database.Close()
}
