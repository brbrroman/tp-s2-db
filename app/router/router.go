package router

import (
	"log"

	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"

	api "tp-s2-db/app/handlers"
	"tp-s2-db/app/middleware"
)

func Start(port string) error {
	log.Println("Api running on port", port)

	var router = fasthttprouter.New()
	router.POST("/api/forum/:slug", middleware.JSON(api.ForumCreate))             // Создание форума
	router.POST("/api/forum/:slug/create", middleware.JSON(api.ForumSlugCreate))  // Добавление новой ветки обсуждения на форум
	router.GET("/api/forum/:slug/details", middleware.JSON(api.ForumSlugDetails)) // Получение информации о форуме по его идентификаторе
	router.GET("/api/forum/:slug/threads", middleware.JSON(api.ForumSlugThreads)) // Получение списка ветвей обсужления данного форума. Ветви обсуждения выводятся отсортированные по дате создания.
	router.GET("/api/forum/:slug/users", middleware.JSON(api.ForumSlugUsers))     // Получение списка пользователей, у которых есть пост или ветка обсуждения в данном форуме. Пользователи выводятся отсортированные по nickname в порядке возрастания. Порядок сотрировки должен соответсвовать побайтовому сравнение в нижнем регистре.

	router.GET("/api/post/:id/details", middleware.JSON(api.PostDetailsGET))   // Получение информации о ветке обсуждения по его имени
	router.POST("/api/post/:id/details", middleware.JSON(api.PostDetailsPOST)) // Изменение сообщения. Если сообщение поменяло текст, то оно должно получить отметку isEdited.

	router.POST("/api/service/clear", middleware.JSON(api.ServiceClear))  // Безвозвратное удаление всей пользовательской информации из базы данных
	router.GET("/api/service/status", middleware.JSON(api.ServiceStatus)) // Получение инфомарции о базе данных

	router.POST("/api/thread/:slug_or_id/create", middleware.JSON(api.ThreadCreate))       // Добавление новых постов в ветку обсуждения на форум.
	router.GET("/api/thread/:slug_or_id/details", middleware.JSON(api.ThreadDetailsGET))   // Получение информации о ветке обсуждения по его имени
	router.POST("/api/thread/:slug_or_id/details", middleware.JSON(api.ThreadDetailsPOST)) // Обновление ветки обсуждения на форуме
	router.GET("/api/thread/:slug_or_id/posts", middleware.JSON(api.ThreadPosts))          // Сообщения данной ветви обсуждения. Сообщения выводятся отсортированные по дате создания.
	router.POST("/api/thread/:slug_or_id/vote", middleware.JSON(api.ThreadVote))           // Изменение голоса за ветвь обсуждения. Один пользователь учитывается только один раз и может изменить своё мнение.

	router.POST("/api/user/:nickname/create", middleware.JSON(api.UserCreate))  // Создание нового пользователя
	router.POST("/api/user/:nickname/profile", middleware.JSON(api.UserUpdate)) // Получение информации о пользователе форума по его имени
	router.GET("/api/user/:nickname/profile", middleware.JSON(api.UserInfo))    // Изменение информации в профиле пользователя

	return fasthttp.ListenAndServe(":"+port, router.Handler)
}
