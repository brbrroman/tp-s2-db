package main

import (
	"log"
	"os"

	"tp-s2-db/app/db"
	"tp-s2-db/app/router"

	"github.com/joho/godotenv"
)

func main() {
	godotenv.Load()

	if err := db.Open(); err != nil {
		log.Fatalln(err.Error())
	}
	defer db.Close()

	port := os.Getenv("PORT")
	if err := router.Start(port); err != nil {
		log.Fatalln(err.Error())
	}
}
