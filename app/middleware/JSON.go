package middleware

import (
	"github.com/valyala/fasthttp"
)

func JSON(next fasthttp.RequestHandler) fasthttp.RequestHandler {
	return fasthttp.RequestHandler(func(ctx *fasthttp.RequestCtx) {
		ctx.SetContentType("application/json")
		next(ctx)
	})
}
