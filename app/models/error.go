package models

import "errors"

var EForumAE = errors.New("Forum already exists")
var EThreadAE = errors.New("Thread already exists")
var EPostAE = errors.New("Post already exists")
var EUserAE = errors.New("User already exists")
var ENE = errors.New("Not Exist")

var EForumNE = errors.New("No Forum")
var EThreadNE = errors.New("No Thread")
var EPostNE = errors.New("No Post")
var EUserNE = errors.New("No User")

var EWrongParent = errors.New("Parent not found")
