package models

import "time"

type Thread struct {
	User    *string    `json:"author"`
	Created *time.Time `json:"created"`
	Slug    *string    `json:"slug,omitempty"`
	Forum   *string    `json:"forum"`
	Message *string    `json:"message"`
	Title   *string    `json:"title"`
	Id      int        `json:"id"`
	Votes   int        `json:"votes"`
}

type Threads []*Thread
