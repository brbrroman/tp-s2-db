package models

type Post struct {
	Author   *string `json:"author"`
	Created  *string `json:"created"`
	Id       int     `json:"id"`
	IsEdited bool    `json:"isEdited"`
	Forum    *string `json:"forum"`
	Message  string  `json:"message"`
	Parent   int     `json:"parent"`
	Thread   int     `json:"thread"`
	Path     []int64 `json:"path"`
}

type Posts []*Post

type PostDetails struct {
	Author *User   `json:"author,omitempty"`
	Forum  *Forum  `json:"forum,omitempty"`
	Post   *Post   `json:"post,omitempty"`
	Thread *Thread `json:"thread,omitempty"`
}
