package helpers

import (
	"bytes"
	"strconv"
	"tp-s2-db/app/db"
	"tp-s2-db/app/models"

	"github.com/jackc/pgx"
)

func DBPostsGetByThreadSlugOrID(orderType *[]byte, slugOrID string, limit []byte, since []byte, desc []byte) (*models.Posts, error) {
	thread, err := strconv.Atoi(slugOrID)
	var forum string
	var row *pgx.Row
	if err != nil {
		row = db.Database.QueryRow("sqlSelectThreadIDForumBySlug", slugOrID)
	} else {
		row = db.Database.QueryRow("sqlSelectThreadIDForumById", thread)
	}
	if err = row.Scan(&thread, &forum); err != nil {
		return nil, models.EThreadNE
	}

	var rows *pgx.Rows
	switch {
	case bytes.Equal(*orderType, []byte("parent_tree")):
		rows, err = DBPostsGetByThreadSlugOrIDParentTree(thread, forum, limit, since, desc)
	case bytes.Equal(*orderType, []byte("tree")):
		rows, err = DBPostsGetByThreadSlugOrIDTree(thread, forum, limit, since, desc)
	default:
		rows, err = DBPostsGetByThreadSlugOrIDFlat(thread, forum, limit, since, desc)
	}
	posts := models.Posts{}
	if err == nil {
		defer rows.Close()
		for rows.Next() {
			var tmpPost models.Post
			tmpPost.Thread = thread
			tmpPost.Forum = &forum
			tmpPost.Created = &credate
			if err := rows.Scan(
				&tmpPost.Author,
				// &tmpPost.Created,
				&tmpPost.Id,
				&tmpPost.Message,
				&tmpPost.Parent); err != nil {
				return nil, err
			}
			posts = append(posts, &tmpPost)
		}
		return &posts, nil
	}
	return nil, err
}
