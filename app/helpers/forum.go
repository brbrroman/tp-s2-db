package helpers

import (
	"tp-s2-db/app/db"
	"tp-s2-db/app/models"

	"github.com/jackc/pgx"
)

// DBForumCreate - создание форума
func DBForumCreate(forum *models.Forum) (*models.Forum, error) {
	var row *pgx.Row
	row = db.Database.QueryRow("sqlSelectUserNicknameByNickname", forum.User)
	if err := row.Scan(&forum.User); err != nil {
		return nil, models.EUserNE
	}

	if err := db.Database.QueryRow("sqlSelectForumBySlug", forum.Slug).Scan(
		&forum.Slug,
		&forum.Title,
		&forum.User,
		&forum.Threads,
		&forum.Posts); err == nil {
		return forum, models.EForumAE
	}

	if _, err := db.Database.Exec("sqlInsertForum", forum.Slug, forum.Title, forum.User); err != nil {
		return nil, err
	}
	forum.Threads = 0
	forum.Posts = 0
	return forum, nil
}

// DBForumBySlug - вернуть форум по пути
func DBForumBySlug(slug string) (*models.Forum, error) {
	var forum models.Forum
	if err := db.Database.QueryRow("sqlSelectForumBySlug", slug).Scan(
		&forum.Slug,
		&forum.Title,
		&forum.User,
		&forum.Threads,
		&forum.Posts); err != nil {
		return nil, models.EForumNE
	}
	return &forum, nil
}
