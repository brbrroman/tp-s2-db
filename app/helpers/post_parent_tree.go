package helpers

import (
	"bytes"
	"tp-s2-db/app/db"

	"github.com/jackc/pgx"
)

// DBPostsGetByThreadSlugOrIDParentTree - Вернуть посты ветки в древовидном порядке с ограничением по родителям
func DBPostsGetByThreadSlugOrIDParentTree(thread int, forum string, limit []byte, since []byte, desc []byte) (*pgx.Rows, error) {
	var rows *pgx.Rows
	var err error
	if since != nil {
		if bytes.Equal(desc, []byte("true")) {
			rows, err = db.Database.Query("sqlSelectPostsParentTreeByIDSinceDESC", thread, since, limit)
		} else {
			rows, err = db.Database.Query("sqlSelectPostsParentTreeByIDSince", thread, since, limit)
		}
	} else {
		if bytes.Equal(desc, []byte("true")) {
			rows, err = db.Database.Query("sqlSelectPostsParentTreeByIDDESC", thread, limit)
		} else {
			rows, err = db.Database.Query("sqlSelectPostsParentTreeByID", thread, limit)
		}
	}
	return rows, err
}
