package helpers

import (
	"tp-s2-db/app/db"
	"tp-s2-db/app/models"
)

// DBStatus - Вернуть статистику по БД
func DBStatus() (*models.Status, error) {
	var status models.Status
	if err := db.Database.QueryRow("sqlStat").Scan(
		&status.Post,
		&status.Thread,
		&status.User,
		&status.Forum); err != nil {
		return nil, err
	}
	return &status, nil
}

// DBClear - Удалить все записи в БД
func DBClear() {
	_, _ = db.Database.Exec("sqlClear")
}
