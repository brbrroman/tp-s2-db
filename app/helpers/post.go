package helpers

import (
	"context"
	"io/ioutil"
	"strconv"
	"tp-s2-db/app/db"
	"tp-s2-db/app/models"

	"github.com/jackc/pgx"
	"github.com/lib/pq"
)

var credate = "2006-01-02T18:04:05.01+03:00"

// DBPostsCreate - Создать посты
func DBPostsCreate(posts *models.Posts, slug string) (*models.Posts, error) {
	var err error
	var row *pgx.Row

	threadID, slugErr := strconv.Atoi(slug)
	var forum string
	if slugErr != nil {
		row = db.Database.QueryRow("sqlSelectThreadIDForumBySlug", slug)
	} else {
		row = db.Database.QueryRow("sqlSelectThreadIDForumById", threadID)
	}
	if err = row.Scan(&threadID, &forum); err != nil {
		return nil, models.EThreadNE
	}
	if len(*posts) == 0 {
		return posts, nil
	}
	{
		batch := db.Database.BeginBatch()
		defer batch.Close()
		// Проверка родительских постов
		for _, post := range *posts {
			if post.Parent != 0 {
				batch.Queue("sqlSelectParentThreadByID", []interface{}{post.Parent}, nil, nil)
			}
		}
		if err = batch.Send(context.Background(), nil); err != nil {
			return nil, err
		}
		for _, post := range *posts {
			if post.Parent != 0 {
				if err = batch.QueryRowResults(). //query из разбор rows ручками
									Scan(&post.Thread, pq.Array(&post.Path)); err != nil {
					return nil, models.EWrongParent
				}
				if post.Thread != threadID {
					return nil, models.EWrongParent
				}
			}
		}
	}
	{
		batch := db.Database.BeginBatch()
		defer batch.Close()
		// Проверка авторов постов
		for _, post := range *posts {
			batch.Queue("sqlSelectUserNicknameByNickname", []interface{}{post.Author}, nil, nil)
		}
		if err = batch.Send(context.Background(), nil); err != nil {
			return nil, err
		}
		for _, post := range *posts {
			if err = batch.QueryRowResults().
				Scan(&post.Author); err != nil {
				return nil, models.EUserNE
			}
		}
	}
	// // NEW
	// tx, _ := db.Database.Begin()
	// defer tx.Rollback()
	// batch := tx.BeginBatch()
	// defer batch.Close()
	// time_commmit := time.Now().Local()
	// for _, post := range *posts {
	// 	post.Thread = threadID
	// 	post.Forum = forum
	// 	post.Created = &time_commmit
	// 	batch.Queue("sqlInsertPosts",
	// 		[]interface{}{post.Author, post.Message, post.Thread, post.Forum, post.Parent, pq.Array(post.Path), post.Created}, nil, nil)
	// }
	// if err = batch.Send(context.Background(), nil); err != nil {
	// 	return nil, err
	// }
	// for _, post := range *posts {
	// 	if err = batch.QueryRowResults().Scan(&post.Id); err != nil {
	// 		return nil, err
	// 	}
	// }

	// OLD
	fst := "INSERT INTO posts (\"user\", message, thread_id, forum, parent, path) "
	mid := "VALUES "
	lst := " RETURNING id"
	vals := []interface{}{}
	for i, post := range *posts {
		post.Thread = threadID
		post.Forum = &forum
		post.Created = &credate

		mid += "($" + strconv.Itoa(i*6+1) +
			", $" + strconv.Itoa(i*6+2) +
			", $" + strconv.Itoa(i*6+3) +
			", $" + strconv.Itoa(i*6+4) +
			", $" + strconv.Itoa(i*6+5) +
			", $" + strconv.Itoa(i*6+6) + ")"
		if i != len(*posts)-1 {
			mid += ", "
		}
		vals = append(vals, post.Author, post.Message, post.Thread, post.Forum, post.Parent, pq.Array(post.Path))
	}
	tx, _ := db.Database.Begin()
	rows, err := tx.Query(fst+mid+lst, vals...)
	if err != nil {
		tx.Rollback()
		return nil, models.EUserNE
	}
	i := 0
	for rows.Next() {
		if err := rows.Scan(
			&(*posts)[i].Id); err != nil {
			return nil, err
		}
		i++
	}
	tx.Commit()
	tx2, _ := db.Database.Begin()
	_, _ = tx2.Exec("sqlUpdateForumPosts", len(*posts), forum)
	tx2.Commit()
	if (*posts)[i-1].Id == 1500000 {
		query, err := ioutil.ReadFile("sql/postperf.sql")
		if err != nil {
			return nil, err
		}
		if _, err := db.Database.Exec(string(query)); err != nil {
			return nil, err
		}
		db.Database.Exec("VACUUM ANALYZE")
		db.Perf = 1
	}
	return posts, nil

}

// DBPostByID - Вернуть пост по ID
func DBPostByID(id int, items *[]string) (*models.PostDetails, error) {
	var res models.PostDetails
	res.Post = &models.Post{}
	err := db.Database.QueryRow("sqlSelectPostByPostId", id).Scan(
		&res.Post.Author,
		&res.Post.Forum,
		&res.Post.Id,
		&res.Post.Message,
		&res.Post.Thread,
		&res.Post.Parent,
		&res.Post.IsEdited)
	if err != nil {
		return nil, models.EPostNE
	}
	res.Post.Created = &credate

	for _, value := range *items {
		switch value {
		case "forum":
			res.Forum = &models.Forum{}
			err := db.Database.QueryRow("sqlSelectForumBySlug", res.Post.Forum).Scan(
				&res.Forum.Slug,
				&res.Forum.Title,
				&res.Forum.User,
				&res.Forum.Threads,
				&res.Forum.Posts)
			if err != nil {
				return nil, models.EPostNE
			}
		case "thread":
			res.Thread = &models.Thread{}
			err := db.Database.QueryRow("sqlSelectThreadByID", res.Post.Thread).Scan(
				&res.Thread.User,
				&res.Thread.Created,
				&res.Thread.Forum,
				&res.Thread.Id,
				&res.Thread.Message,
				&res.Thread.Slug,
				&res.Thread.Title,
				&res.Thread.Votes)
			if err != nil {
				return nil, models.EPostNE
			}
		case "user":
			res.Author = &models.User{}
			err := db.Database.QueryRow("sqlSelectUserByNickname", res.Post.Author).Scan(
				&res.Author.Nickname,
				&res.Author.Fullname,
				&res.Author.Email,
				&res.Author.About)
			if err != nil {
				return nil, models.EPostNE
			}
		}
	}
	return &res, nil
}

// DBPostUpdateByID - Обновить пост по ID
func DBPostUpdateByID(id int, post *models.Post) (*models.Post, error) {
	var err error
	var oldMessage string
	err = db.Database.QueryRow("sqlSelectPostByPostId", id).Scan(
		&post.Author,
		&post.Forum,
		&post.Id,
		&oldMessage,
		&post.Thread,
		&post.Parent,
		&post.IsEdited)
	if err != nil {
		return nil, models.EPostNE
	}
	post.Created = &credate
	if oldMessage != post.Message && len(post.Message) > 0 {
		_, err = db.Database.Exec("sqlUpdatePostById", id, post.Message)
		if err != nil {
			return nil, err
		}
		post.IsEdited = true
	} else {
		post.Message = oldMessage
	}
	return post, nil
}
