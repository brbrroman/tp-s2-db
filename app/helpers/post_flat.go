package helpers

import (
	"bytes"
	"tp-s2-db/app/db"

	"github.com/jackc/pgx"
)

// DBPostsGetByThreadSlugOrIDFlat - Вернуть посты ветки в простом порядке
func DBPostsGetByThreadSlugOrIDFlat(thread int, forum string, limit []byte, since []byte, desc []byte) (*pgx.Rows, error) {
	var rows *pgx.Rows
	var err error
	if since != nil {
		if bytes.Equal(desc, []byte("true")) {
			rows, err = db.Database.Query("sqlSelectPostsByThreadIdSinceDESC", thread, since, limit)
		} else {
			rows, err = db.Database.Query("sqlSelectPostsByThreadIdSince", thread, since, limit)
		}
	} else {
		if bytes.Equal(desc, []byte("true")) {
			rows, err = db.Database.Query("sqlSelectPostsByThreadIdFlatDESC", thread, limit)
		} else {
			rows, err = db.Database.Query("sqlSelectPostsByThreadIdFlat", thread, limit)
		}
	}
	return rows, err
}
