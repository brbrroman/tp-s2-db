package helpers

import (
	"bytes"
	"strconv"
	"time"

	"tp-s2-db/app/db"
	"tp-s2-db/app/models"

	"github.com/jackc/pgx"
)

// RFC3339Milli - Константа формата времени
const RFC3339Milli = "2006-01-02T15:04:05.000Z07:00"

// DBThreadCreate - Создать ветку
func DBThreadCreate(thread *models.Thread) (*models.Thread, error) {
	row := db.Database.QueryRow("sqlSelectForumSlugBySlug", thread.Forum)
	if err := row.Scan(&thread.Forum); err != nil {
		return nil, models.EForumNE
	}

	row = db.Database.QueryRow("sqlSelectUserNicknameByNickname", thread.User)
	if err := row.Scan(&thread.User); err != nil {
		return nil, models.EUserNE
	}

	row = db.Database.QueryRow("sqlSelectThreadBySlug", thread.Slug)
	if err := row.Scan(&thread.User,
		&thread.Created,
		&thread.Forum,
		&thread.Id,
		&thread.Message,
		&thread.Slug,
		&thread.Title,
		&thread.Votes); err == nil {
		return thread, models.EThreadAE
	}

	row = db.Database.QueryRow("sqlInsertThread", thread.Created, thread.Message, thread.Title, thread.User, thread.Forum, thread.Slug)
	if err := row.Scan(&thread.Id); err != nil {
		return nil, err
	}
	return thread, nil
}

// DBThreadsBySlugLimit - Вернуть ветки форума
func DBThreadsBySlugLimit(slug string, limit []byte, tmpsince []byte, desc []byte) (*models.Threads, error) {
	var err error
	var row *pgx.Row
	var forum string
	row = db.Database.QueryRow("sqlSelectForumSlugBySlug", slug)
	if err = row.Scan(&forum); err != nil {
		return nil, models.EForumNE
	}
	var rows *pgx.Rows
	if tmpsince != nil {
		since, err := time.Parse(RFC3339Milli, string(tmpsince))
		if err != nil {
			return nil, models.EPostNE
		}
		if bytes.Equal(desc, []byte("true")) {
			rows, err = db.Database.Query("sqlSelectThreadByForumSinceDESC", forum, since, limit)
		} else {
			rows, err = db.Database.Query("sqlSelectThreadByForumSince", forum, since, limit)
		}
	} else {
		if bytes.Equal(desc, []byte("true")) {
			rows, err = db.Database.Query("sqlSelectThreadByForumDESC", forum, limit)
		} else {
			rows, err = db.Database.Query("sqlSelectThreadByForum", forum, limit)
		}
	}

	threads := models.Threads{}
	if err == nil {
		defer rows.Close()
		for rows.Next() {
			var tmpThread models.Thread
			tmpThread.Forum = &forum
			if err := rows.Scan(
				&tmpThread.User,
				&tmpThread.Created,
				&tmpThread.Id,
				&tmpThread.Message,
				&tmpThread.Slug,
				&tmpThread.Title,
				&tmpThread.Votes); err != nil {
				return nil, err
			}
			threads = append(threads, &tmpThread)
		}
		return &threads, nil
	}
	return nil, err
}

// DBUsersBySlugLimit - Вернуть пользователей форума
func DBUsersBySlugLimit(slug string, limit []byte, tmpsince []byte, desc []byte) (*models.Users, error) {
	var err error
	var row *pgx.Row
	var forum string
	row = db.Database.QueryRow("sqlSelectForumSlugBySlug", slug)
	if err = row.Scan(&forum); err != nil {
		return nil, models.EForumNE
	}

	var rows *pgx.Rows
	if db.Perf == 0 {
		if tmpsince != nil {
			since := string(tmpsince)
			if bytes.Equal(desc, []byte("true")) {
				rows, err = db.Database.Query("sqlSelectUsersByForumSinceDESC", forum, since, limit)
			} else {
				rows, err = db.Database.Query("sqlSelectUsersByForumSince", forum, since, limit)
			}
		} else {
			if bytes.Equal(desc, []byte("true")) {
				rows, err = db.Database.Query("sqlSelectUsersByForumDESC", forum, limit)
			} else {
				rows, err = db.Database.Query("sqlSelectUsersByForum", forum, limit)
			}
		}

	} else {
		if tmpsince != nil {
			since := string(tmpsince)
			if bytes.Equal(desc, []byte("true")) {
				rows, err = db.Database.Query("sqlSelectUsersByForumSinceDESC2", since, limit)
			} else {
				rows, err = db.Database.Query("sqlSelectUsersByForumSince2", since, limit)
			}
		} else {
			if bytes.Equal(desc, []byte("true")) {
				rows, err = db.Database.Query("sqlSelectUsersByForumDESC2", limit)
			} else {
				rows, err = db.Database.Query("sqlSelectUsersByForum2", limit)
			}
		}
	}

	users := models.Users{}
	if err == nil {
		defer rows.Close()
		for rows.Next() {
			var tmpUser models.User
			if err := rows.Scan(
				&tmpUser.Nickname,
				&tmpUser.Email,
				&tmpUser.Fullname,
				&tmpUser.About); err != nil {
				return nil, err
			}
			users = append(users, &tmpUser)
		}
		return &users, nil
	}
	return nil, err
}

// DBVote - Создать/Обновить голос у ветки
func DBVote(slug string, vote *models.Vote) (*models.Thread, error) {
	threadID, slugErr := strconv.Atoi(slug)
	var thread models.Thread
	var row *pgx.Row

	if slugErr != nil {
		row = db.Database.QueryRow("sqlSelectThreadIDForumBySlug", slug)
	} else {
		row = db.Database.QueryRow("sqlSelectThreadIDForumById", threadID)
	}

	if err := row.Scan(&vote.Thread, &thread.Forum); err != nil {
		return nil, models.EThreadNE
	}

	row = db.Database.QueryRow("sqlSelectUserNicknameByNickname", vote.Nickname)
	if err := row.Scan(&vote.Nickname); err != nil {
		return nil, models.EUserNE
	}

	if _, err := db.Database.Exec("sqlInsertVoteByID", vote.Nickname, vote.Thread, vote.Voice); err != nil {
		return nil, err
	}
	row = db.Database.QueryRow("sqlSelectThreadByID", vote.Thread)
	err := row.Scan(
		&thread.User,
		&thread.Created,
		&thread.Forum,
		&thread.Id,
		&thread.Message,
		&thread.Slug,
		&thread.Title,
		&thread.Votes)
	if err != nil {
		return nil, err //models.NotExist
	}

	return &thread, nil
}

// DBThreadBySlug - Вернуть ветку по пути
func DBThreadBySlug(slug string) (*models.Thread, error) {
	var thread models.Thread
	threadID, slugerr := strconv.Atoi(slug)
	var row *pgx.Row
	if slugerr != nil { // slug
		row = db.Database.QueryRow("sqlSelectThreadBySlug", slug)
	} else {
		row = db.Database.QueryRow("sqlSelectThreadByID", threadID)
	}
	err := row.Scan(
		&thread.User,
		&thread.Created,
		&thread.Forum,
		&thread.Id,
		&thread.Message,
		&thread.Slug,
		&thread.Title,
		&thread.Votes)
	if err != nil {
		return nil, models.EThreadNE
	}

	return &thread, nil
}

// DBUpdateThreadBySlug - Обновит ветку
func DBUpdateThreadBySlug(slug string, thread *models.Thread) (*models.Thread, error) {
	threadID, slugerr := strconv.Atoi(slug)
	var row *pgx.Row
	if slugerr != nil { // slug
		row = db.Database.QueryRow("sqlUpdateThreadBySlug", slug, thread.Message, thread.Title)
	} else {
		row = db.Database.QueryRow("sqlUpdateThreadByID", threadID, thread.Message, thread.Title)
	}
	err := row.Scan(
		&thread.User,
		&thread.Created,
		&thread.Forum,
		&thread.Id,
		&thread.Message,
		&thread.Slug,
		&thread.Title,
		&thread.Votes)
	if err != nil {
		return nil, models.EThreadNE
	}

	return thread, nil
}
