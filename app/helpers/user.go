package helpers

import (
	"tp-s2-db/app/db"
	"tp-s2-db/app/models"
)

// DBUserCreate - Создать пользователя
func DBUserCreate(user *models.User) (*models.Users, error) {
	users := models.Users{}
	if rows, err := db.Database.Query("sqlSelectUserByNicknameOrEmail", user.Nickname, user.Email); err == nil {
		defer rows.Close()
		for rows.Next() {
			var existingUser models.User
			if err := rows.Scan(
				&existingUser.Nickname,
				&existingUser.Fullname,
				&existingUser.Email,
				&existingUser.About); err != nil {
				return nil, err
			}
			users = append(users, &existingUser)
		}
		if len(users) > 0 {
			return &users, models.EUserAE
		}
	}

	_, err := db.Database.Exec("sqlInsertUser", user.Nickname, user.Fullname, user.Email, user.About)
	if err != nil {
		return nil, err
	}
	users = append(users, user)
	return &users, nil
}

// DBUserByNickname - Вернуть пользователя по нику
func DBUserByNickname(nickname string) (*models.User, error) {
	var user models.User
	if err := db.Database.QueryRow("sqlSelectUserByNickname", nickname).Scan(
		&user.Nickname,
		&user.Fullname,
		&user.Email,
		&user.About); err != nil {
		return nil, models.EUserNE
	}
	return &user, nil
}

// DBUserUpdate - Обновить данные пользователя
func DBUserUpdate(user *models.User) (*models.User, error) {
	row := db.Database.QueryRow("sqlSelectUserNicknameByNickname", user.Nickname)
	if err := row.Scan(&user.Nickname); err != nil {
		return nil, models.EUserNE
	}

	if err := db.Database.QueryRow("sqlUpdateUserByNickname", user.Nickname, user.Fullname, user.Email, user.About).Scan(
		&user.Fullname,
		&user.Email,
		&user.About); err != nil {
		return nil, models.EUserAE
	}
	return user, nil
}
