package helpers

import (
	"bytes"
	"tp-s2-db/app/db"

	"github.com/jackc/pgx"
)

// DBPostsGetByThreadSlugOrIDTree - Вернуть посты ветки в древовидном порядке с общим ограничением
func DBPostsGetByThreadSlugOrIDTree(thread int, forum string, limit []byte, since []byte, desc []byte) (*pgx.Rows, error) {
	var rows *pgx.Rows
	var err error
	if since != nil {
		if bytes.Equal(desc, []byte("true")) {
			rows, err = db.Database.Query("sqlSelectPostsByThreadIdThreeSinceDESC", thread, since, limit)
		} else {
			rows, err = db.Database.Query("sqlSelectPostsByThreadIdThreeSince", thread, since, limit)
		}
	} else {
		if bytes.Equal(desc, []byte("true")) {
			rows, err = db.Database.Query("sqlSelectPostsByThreadIdThreeDESC", thread, limit)
		} else {
			rows, err = db.Database.Query("sqlSelectPostsByThreadIdThree", thread, limit)
		}
	}
	return rows, err
}
