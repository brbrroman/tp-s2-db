-- После заполнения
CREATE INDEX IF NOT EXISTS indx_threads_created ON threads (created, forum);
-- После заполнения
CREATE INDEX IF NOT EXISTS indx_posts_thread_id3 ON posts (thread_id, id);
-- sqlSelectPostsByThreadIdSince
CREATE INDEX IF NOT EXISTS indx_posts_thread_id4 ON posts (thread_id, (path || array[id]), parent);
    -- sqlSelectPostsByThreadIdThreeSince
CREATE INDEX IF NOT EXISTS indx_posts_thread_id3_zero ON posts (thread_id, id) WHERE parent = 0;
    -- sqlSelectPostsParentTreeByIDSince
CREATE INDEX IF NOT EXISTS indx_posts_root ON posts (((path || array[id])[1]))  INCLUDE (id);
    -- sqlSelectPostsParentTreeByIDSince

CLUSTER users USING users_nickname_key;
CLUSTER threads USING indx_threads_created;
CLUSTER forums USING indx_forums_slug;
CLUSTER posts USING indx_posts_thread_id3;