CREATE SCHEMA IF NOT EXISTS public;

DROP SCHEMA IF EXISTS public CASCADE;
CREATE SCHEMA public;

CREATE EXTENSION IF NOT EXISTS CITEXT;

CREATE UNLOGGED TABLE IF NOT EXISTS users (
    nickname    CITEXT UNIQUE COLLATE ucs_basic NOT NULL,
    email       CITEXT UNIQUE NOT NULL,
    fullname    TEXT NOT NULL,
    about       TEXT NOT NULL
) WITH (autovacuum_enabled = off);
-- CREATE INDEX IF NOT EXISTS indx_users_nickname ON users (nickname) INCLUDE (email, fullname, about);

-- Попробовать выделить количественные в отдельную таблицу
CREATE UNLOGGED TABLE IF NOT EXISTS forums (
    slug        CITEXT NOT NULL,
    title       TEXT NOT NULL,
    posts       BIGINT DEFAULT 0 NOT NULL,
    threads     BIGINT DEFAULT 0 NOT NULL,
    "user"      TEXT NOT NULL
) WITH (autovacuum_enabled = off);

CREATE INDEX IF NOT EXISTS indx_forums_slug ON forums (slug) INCLUDE ("user", title, threads, posts);

-- Попробовать выделить количественные в отдельную таблицу
CREATE UNLOGGED TABLE IF NOT EXISTS threads (
    id          SERIAL8 NOT NULL,
    created     TIMESTAMPTZ,
    slug        CITEXT,
    message     TEXT NOT NULL,
    title       TEXT NOT NULL,
    votes       INTEGER DEFAULT 0 NOT NULL,
    "user"      TEXT NOT NULL,
    forum       TEXT NOT NULL
) WITH (autovacuum_enabled = off);

CREATE INDEX IF NOT EXISTS indx_threads_id ON threads (id) INCLUDE (forum);
CREATE INDEX IF NOT EXISTS indx_threads_slug ON threads (slug) INCLUDE (forum, id);

CREATE UNLOGGED TABLE IF NOT EXISTS posts (
    id          SERIAL8 NOT NULL,
    -- created     TIMESTAMPTZ NOT NULL,
    edited      BOOLEAN DEFAULT FALSE NOT NULL,
    message     TEXT NOT NULL,
    "user"      TEXT NOT NULL,
    thread_id   BIGINT NOT NULL,
    forum       TEXT NOT NULL,
    parent      BIGINT NOT NULL,
    path        BIGINT []
) WITH (autovacuum_enabled = off);

CREATE INDEX IF NOT EXISTS indx_posts_id ON posts (id) INCLUDE (path, thread_id);
    -- sqlSelectPostsParentTreeByIDSince

CREATE UNLOGGED TABLE IF NOT EXISTS votes (
    voice         SMALLINT NOT NULL,
    UNIQUE (thread_id, "user"),
    thread_id     BIGINT NOT NULL,
    "user"        CITEXT NOT NULL
) WITH (autovacuum_enabled = off);

CREATE OR REPLACE FUNCTION vote_add() RETURNS TRIGGER AS $emp_audit$
    BEGIN
    UPDATE threads 
    SET votes = votes + NEW.voice
    WHERE id = NEW.thread_id;
    RETURN NULL;
    END;
$emp_audit$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION vote_update() RETURNS TRIGGER AS $emp_audit$
    BEGIN
    UPDATE threads 
    SET votes = votes - OLD.voice + NEW.voice
    WHERE id = OLD.thread_id;
    RETURN NULL;
    END;
$emp_audit$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION thread_add() RETURNS TRIGGER AS $emp_audit$
    BEGIN
    UPDATE forums 
    SET threads = threads + 1
    WHERE slug = NEW.forum;
    RETURN NULL;
    END;
$emp_audit$ LANGUAGE plpgsql;

-- Votes
DROP TRIGGER IF EXISTS vote_insert ON votes;

CREATE TRIGGER vote_insert 
  AFTER INSERT 
  ON votes
  FOR EACH ROW EXECUTE PROCEDURE vote_add();
  
DROP TRIGGER IF EXISTS vote_update ON votes;

CREATE TRIGGER vote_update
  AFTER UPDATE 
  ON votes
  FOR EACH ROW EXECUTE PROCEDURE vote_update();

-- Threads
DROP TRIGGER IF EXISTS thread_insert ON threads;

CREATE TRIGGER thread_insert 
  AFTER INSERT 
  ON threads
  FOR EACH ROW EXECUTE PROCEDURE thread_add();
  
