module tp-s2-db

go 1.12

require (
	github.com/buaazp/fasthttprouter v0.1.1
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.3.0+incompatible
	github.com/joho/godotenv v1.3.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.0.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/pquerna/ffjson v0.0.0-20181028064349-e517b90714f7
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/shopspring/decimal v0.0.0-20180709203117-cd690d0c9e24 // indirect
	github.com/valyala/fasthttp v1.2.0
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
